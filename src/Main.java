import controllers.LocalDiskCController;
import controllers.LocalDiskDController;
import utiliy.Keyboard;

public class Main {

    public static LocalDiskCController localDiskCController = new LocalDiskCController();
    public static LocalDiskDController localDiskDController = new LocalDiskDController();
    public static Keyboard keyboard = new Keyboard();
    public static boolean currentDiskC = true;

    public static void main(String[] args) {
        Main main = new Main();
        main.printCurrentDisk();
        main.menu();
        int option;
        do {
            System.out.print("\nSeleccione una opción en el menú principal: ");
            option = keyboard.readIntegerDefault(-1);
            switch (option) {
                case 0 -> System.out.println(" El programa ha finalizado");
                case 1 -> main.selectDisk();
                case 2 -> main.accessFolder();
                case 3 -> main.addFile();
                case 4 -> main.addFolder();
                case 5 -> main.printContent();
                case 6 -> main.mainPath();
                case 7 -> main.accessFolderByPath();
                default -> System.out.println(" ¡Opción no disponible en el menú principal!");
            }
        } while (option != 0);
    }

    public void menu() {
        System.out.println("╔═══════════════════════════════════════════════════════╗");
        System.out.println("╠------------------Sistema de archivos------------------╣");
        System.out.println("║═══════════════════════════════════════════════════════║");
        System.out.println("║   1. Seleccionar disco                                ║");
        System.out.println("║   2. Acceder a carpeta                                ║");
        System.out.println("║   3. Crear archivo                                    ║");
        System.out.println("║   4. Crear carpeta                                    ║");
        System.out.println("║   5. Imprimir contenido de la carpeta                 ║");
        System.out.println("║   6. Regresar a la ruta raíz                          ║");
        System.out.println("║   7. Acceder por ruta                                 ║");
        System.out.println("║═══════════════════════════════════════════════════════║");
        System.out.println("║   0. Salir                                            ║");
        System.out.println("╚═══════════════════════════════════════════════════════╝");
    }

    public void printCurrentDisk(){
        System.out.println("╔═══════════════════════════════════════════════════════╗");
        System.out.println("    Disco "+(currentDiskC? "del sistema": "de archivos personales"));
        System.out.println("╚═══════════════════════════════════════════════════════╝");
    }

    public void selectDisk(){
        int option = keyboard.readValidBoolean("  Seleccione el disco que desea manejar\n  (1 -> disco del Sistema,  " +
                "2 -> disco de archivos): ");
        currentDiskC = option == 1;
        printCurrentDisk();
    }

    public void accessFolder(){
        System.out.print("  Ingrese el nombre de la carpeta: ");
        if (currentDiskC){
            localDiskCController.accessFolder(keyboard.readLine());
        } else {
            localDiskDController.accessFolder(keyboard.readLine());
        }
    }

    public void addFile(){
        System.out.print("  Ingrese el nombre del archivo: ");
        String fileName = keyboard.readLine();
        System.out.print("  Ingrese el tipo de archivo (Imagen, Documento, Comprimido, Video, Ejecución, " +
                "Audio, Lectura, Sistema): ");
        String fileType = keyboard.readLine();
        if (currentDiskC){
            localDiskCController.addFile(fileName, fileType);
        } else {
            localDiskDController.addFile(fileName, fileType);
        }
    }

    public void addFolder(){
        System.out.print("  Ingrese el nombre de la carpeta que desea crear: ");
        if (currentDiskC){
            localDiskCController.addFolder(keyboard.readLine());
        } else {
            localDiskDController.addFolder(keyboard.readLine());
        }
    }

    public void printContent(){
        if (currentDiskC){
            localDiskCController.printCurrentFolder();
        } else {
            localDiskDController.printCurrentFolder();
        }
    }

    public void mainPath(){
        if (currentDiskC){
            localDiskCController.getMainPath();
        } else {
            localDiskDController.getMainPath();
        }
    }

    public void accessFolderByPath(){
        String path = keyboard.readLineValidDefault("  Ingrese la ruta del archivo (/example/example2): ");
        if (currentDiskC){
            localDiskCController.accessFolderByPath(path);
        } else {
            localDiskDController.accessFolderByPath(path);
        }
    }
}
