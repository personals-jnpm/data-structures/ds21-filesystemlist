package repositories;

import models.File;
import models.Folder;
import models.TypeFile;

public class LocalDiskC {

    private static final String ROOT_FOLDER_NAME = "root";
    private Folder rootFolder;
    private Folder currentFolder;

    public LocalDiskC() {
        rootFolder = new Folder();
        rootFolder.setName(ROOT_FOLDER_NAME);
        rootFolder.setPath("/" + ROOT_FOLDER_NAME);
        currentFolder = rootFolder;
        initContent();
    }

    public Folder getRootFolder() {
        return rootFolder;
    }

    public void setRootFolder(Folder rootFolder) {
        this.rootFolder = rootFolder;
    }

    public Folder getCurrentFolder() {
        return currentFolder;
    }

    public void setCurrentFolder(Folder currentFolder) {
        this.currentFolder = currentFolder;
    }

    public void addFolder(Folder folder) {
        currentFolder.addFolder(folder);
    }

    public void addFile(File file) {
        currentFolder.addFile(file);
    }

    public void accessFolder(String folderName) {
        try {
            currentFolder = currentFolder.getFolderByName(folderName);
            System.out.println("\n  Ruta actual: " + currentFolder.getPath());
        } catch (Exception e) {
            System.out.println(" ¡La ruta ingresada no es válida!");
            getMainPath();
        }
    }

    public void accessFolderByPath(String path) {
        try {
            for (String folder : path.split("/")) {
                currentFolder = currentFolder.getFolderByName(folder);
            }
            System.out.println("\n  Ruta actual: " + currentFolder.getPath());
        } catch (Exception e) {
            System.out.println("  ¡Ruta no válida!");
            getMainPath();
        }
    }

    public void getMainPath() {
        currentFolder = rootFolder;
        System.out.println("\n  Ruta actual: " + currentFolder.getPath());
    }

    private void initContent() {
        rootFolder.addFolder(new Folder("dev"));
        rootFolder.addFolder(new Folder("run"));
        rootFolder.addFolder(new Folder("etc"));
        rootFolder.addFolder(new Folder("var"));
        rootFolder.addFolder(new Folder("bin"));
        rootFolder.addFolder(new Folder("home"));
        rootFolder.addFolder(new Folder("usr"));

        rootFolder.getFolderByName("dev").addFolder(new Folder("block"));
        rootFolder.getFolderByName("dev").addFolder(new Folder("fd"));
        rootFolder.getFolderByName("dev").addFolder(new Folder("disk"));
        rootFolder.getFolderByName("dev").addFile(new File("core", TypeFile.Sistema));

        rootFolder.getFolderByName("run").addFolder(new Folder("avahi-daemon"));
        rootFolder.getFolderByName("run").addFolder(new Folder("docker"));

        rootFolder.getFolderByName("etc").addFolder(new Folder("sudoers.d"));
        rootFolder.getFolderByName("etc").addFile(new File("crypttab", TypeFile.Sistema));

        rootFolder.getFolderByName("bin").addFile(new File("autopoint", TypeFile.Ejecución));
        rootFolder.getFolderByName("bin").addFile(new File("install", TypeFile.Sistema));
        rootFolder.getFolderByName("bin").addFile(new File("Isusb", TypeFile.Ejecución));

        rootFolder.getFolderByName("home").addFolder(new Folder("Jaider"));
        rootFolder.getFolderByName("home").getFolderByName("Jaider").addFolder(new Folder("Downloads"));
        rootFolder.getFolderByName("home").getFolderByName("Jaider").addFolder(new Folder("Images"));
        rootFolder.getFolderByName("home").getFolderByName("Jaider").addFolder(new Folder("Documents"));

        rootFolder.getFolderByName("usr").addFolder(new Folder("bin"));
        rootFolder.getFolderByName("usr").addFolder(new Folder("local"));
        rootFolder.getFolderByName("usr").addFolder(new Folder("src"));
    }
}
