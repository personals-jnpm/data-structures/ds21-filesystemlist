package repositories;

import models.File;
import models.Folder;

public class LocalDiskD {

    private static final String ROOT_FOLDER_NAME = "files";
    private Folder rootFolder;
    private Folder currentFolder;

    public LocalDiskD() {
        rootFolder = new Folder();
        rootFolder.setName(ROOT_FOLDER_NAME);
        rootFolder.setPath("/" + ROOT_FOLDER_NAME);
        currentFolder = rootFolder;
    }

    public Folder getRootFolder() {
        return rootFolder;
    }

    public void setRootFolder(Folder rootFolder) {
        this.rootFolder = rootFolder;
    }

    public Folder getCurrentFolder() {
        return currentFolder;
    }

    public void setCurrentFolder(Folder currentFolder) {
        this.currentFolder = currentFolder;
    }

    public void addFolder(Folder folder) {
        currentFolder.addFolder(folder);
    }

    public void addFile(File file) {
        currentFolder.addFile(file);
    }

    public void accessFolder(String folderName) {
        try {
            currentFolder = currentFolder.getFolderByName(folderName);
            System.out.println("\n  Ruta actual: " + currentFolder.getPath());
        } catch (Exception e) {
            System.out.println(" ¡La ruta ingresada no es válida!");
            getMainPath();
        }
    }

    public void accessFolderByPath(String path) {
        try {
            for (String folder : path.split("/")) {
                currentFolder = currentFolder.getFolderByName(folder);
            }
            System.out.println("\n  Ruta actual: " + currentFolder.getPath());
        } catch (Exception e) {
            System.out.println(" ¡Ruta no válida!");
            getMainPath();
        }
    }

    public void getMainPath() {
        currentFolder = rootFolder;
        System.out.println("\n  Ruta actual: " + currentFolder.getPath());
    }
}
