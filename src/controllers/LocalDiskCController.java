package controllers;

import models.File;
import models.Folder;
import models.TypeFile;
import repositories.LocalDiskC;

public class LocalDiskCController {

    private LocalDiskC localDiskC;

    public LocalDiskCController() {
        localDiskC = new LocalDiskC();
    }

    public LocalDiskC getLocalDiskC() {
        return localDiskC;
    }

    public void setLocalDiskC(LocalDiskC localDiskC) {
        this.localDiskC = localDiskC;
    }

    public void addFolder(String folderName){
       localDiskC.addFolder(new Folder(folderName));
    }

    public void addFile(String fileName, String fileType){
        localDiskC.addFile(new File(fileName, getTypeFile(fileType)));
    }

    public void accessFolder(String folderName){
        localDiskC.accessFolder(folderName);
    }

    public void getMainPath(){
        localDiskC.getMainPath();
    }

    public void printCurrentFolder(){
        localDiskC.getCurrentFolder().printContent();
    }

    public void accessFolderByPath(String path){
        localDiskC.accessFolderByPath(path.substring(1));
    }

    private TypeFile getTypeFile(String option)  {
        switch (option) {
            case "Imagen", "imagen" -> {
                return TypeFile.Imagen;
            }
            case "Sistema", "sistema" -> {
                return TypeFile.Sistema;
            }
            case "Ejecución", "ejecución" ->{
                return TypeFile.Ejecución;
            }
            case "Documento", "documento" ->{
                return TypeFile.Documento;
            }
            case "Audio", "audio" ->{
                return TypeFile.Audio;
            }
            case "Comprimido", "comprimido" ->{
                return TypeFile.Comprimido;
            }
            case "Lectura", "lectura" ->{
                return TypeFile.Lectura;
            }
            case "Video", "video" ->{
                return TypeFile.Video;
            }
            default -> {
                return TypeFile.SinDefinir;
            }
        }
    }
}
