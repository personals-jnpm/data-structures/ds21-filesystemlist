package controllers;

import models.File;
import models.Folder;
import models.TypeFile;
import repositories.LocalDiskD;

public class LocalDiskDController {

    private LocalDiskD localDiskD;

    public LocalDiskDController() {
        localDiskD = new LocalDiskD();
    }

    public LocalDiskD getLocalDiskD() {
        return localDiskD;
    }

    public void setLocalDiskD(LocalDiskD localDiskD) {
        this.localDiskD = localDiskD;
    }

    public void addFolder(String folderName){
        localDiskD.addFolder(new Folder(folderName));
    }

    public void addFile(String fileName, String fileType){
        localDiskD.addFile(new File(fileName, getTypeFile(fileType)));
    }

    public void accessFolder(String folderName){
        localDiskD.accessFolder(folderName);
    }

    public void getMainPath(){
        localDiskD.getMainPath();
    }

    public void printCurrentFolder(){
        localDiskD.getCurrentFolder().printContent();
    }

    public void accessFolderByPath(String path){
        localDiskD.accessFolderByPath(path.substring(1));
    }

    private TypeFile getTypeFile(String option)  {
        switch (option) {
            case "Imagen", "imagen" -> {
                return TypeFile.Imagen;
            }
            case "Sistema", "sistema" -> {
                return TypeFile.Sistema;
            }
            case "Ejecución", "ejecución" ->{
                return TypeFile.Ejecución;
            }
            case "Documento", "documento" ->{
                return TypeFile.Documento;
            }
            case "Audio", "audio" ->{
                return TypeFile.Audio;
            }
            case "Comprimido", "comprimido" ->{
                return TypeFile.Comprimido;
            }
            case "Lectura", "lectura" ->{
                return TypeFile.Lectura;
            }
            case "Video", "video" ->{
                return TypeFile.Video;
            }
            default -> {
                return TypeFile.SinDefinir;
            }
        }
    }

}
