package models;

public class File {

    private String fileName;
    private TypeFile typeFile;

    public File(String fileName, TypeFile typeFile) {
        this.fileName = fileName;
        this.typeFile = typeFile;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public TypeFile getTypeFile() {
        return typeFile;
    }

    public void setTypeFile(TypeFile typeFile) {
        this.typeFile = typeFile;
    }

    @Override
    public String toString() {
        return "File {" +
                "Nombre de archivo: " + fileName + ",  " +
                "Tipo de archivo: " + typeFile +
                "}";
    }
}
