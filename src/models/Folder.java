package models;

import java.util.ArrayList;
import java.util.List;

public class Folder {

    private String path;
    private String name;
    private List<File> files;
    private List<Folder> folders;

    public Folder() {
        initFolder();
    }

    public Folder(String name) {
        this.name = name;
        initFolder();
    }

    public void initFolder() {
        files = new ArrayList<>();
        folders = new ArrayList<>();
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<File> getFiles() {
        return files;
    }

    public void setFiles(List<File> files) {
        this.files = files;
    }

    public List<Folder> getFolders() {
        return folders;
    }

    public void setFolders(List<Folder> folders) {
        this.folders = folders;
    }

    public void addFile(File file) {
        files.add(file);
    }

    public void addFolder(Folder folder) {
        folder.setPath(path + "/" + folder.getName());
        folders.add(folder);
    }

    public Folder getFolderByName(String name) {
        for (Folder folder : folders) {
            if (folder.getName().equals(name)) return folder;
        }
        return null;
    }

    public void printContent() {
        System.out.println("\nContenido de la carpeta " + name + " [");
        for (Folder folder : folders) {
            System.out.println("\t" + folder);
        }
        for (File file : files) {
            System.out.println("\t" + file);
        }
        System.out.println("]");
    }

    @Override
    public String toString() {
        return "Folder {" +
                "nombre: " + name +
                "}";
    }
}
