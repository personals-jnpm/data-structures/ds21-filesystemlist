package models;

public enum TypeFile {
    Imagen, Documento, Comprimido, Video, Ejecución, Audio, Lectura, Sistema, SinDefinir
}
